#!/usr/bin/env python3
# -*- coding: utf-8 -*-
  
from tkinter import *
import random
 
fenetre = Tk()
fenetre.title("QCM")
 
cadre = Frame(fenetre)
cadre.pack
 
lChx = 0
 
Question = [["Quelle est la capitale de la France ?","A : Nantes","B : Paris","C : Marseilles","B : Paris"],
            ["Quel est le plus grand ocean du monde ? ","A : Le Pacifique","B : L'Atlantique","C : La mer Noire","A : Le Pacifique"],
            ["Avec quel pays la France n'a t-elle aucune frontière ?","A : Autriche","B : Belgique","C : Monaco","A : Autriche"],
            ["Dans quelle ville ce trouve le pont Vasco de Gamma ?","A : Madrid","B : Barcelone","C : Lisbonne","C : Lisbonne"],
            ["Quelle est la capitale des Phillipines ?","A : Manille","B : Jakarta","C : Wellington","A : Manille"] ,
            ["Quel département français a pour code géographique le 16 ? ","A : Le Cantal","B : La Charente","C : L'Aude","B : La Charente"],
            ["Dans quel pays se trouve Phnom Penh ?","A : Chine","B : Corée du Nord","C : Cambodge","C : Cambodge"],
            ["Les habitants de Jerusalem se nomment les ?","A : Yerousalemiens","B : Hierosolymitains","C : Jerusalemistes","B : Hierosolymitains"],
            [" La France compte environ ","A : 37 256 communes ?","B : 25 894 communes ? ","C : 36 681 communes ?","C : 36 681 communes ?"],
            ["La superficie du Bandgladesh est de ? ","A : 150 254 Km2","B : 147 570 km2","C : 80 456 km2","B : 147 570 km2"]]
 
def RepA (self):
     global lChx
     if lRep == "A": #On récupere le choix
         lChx = lQst[1]
 
def RepB (self):
     global lChx
     if lRep == "B": #On récupere le choix
         lChx = lQst[2]
 
def RepC (self):
     global lChx
     if lRep == "C": #On récupere le choix
         lChx = lQst[3]     
 
def verification():
     if lChx == lQst[4]: #rpvrai
        true = Label(fenetre, text = "--VRAI--")
        true.pack()
        question.destroy()
        bouton1.destroy()
        bouton2.destroy()
        bouton3.destroy()
        valider.destroy()
        fermer.destroy()
     else : #Sinon
        false = Label(fenetre, text = "--FAUX--")
        false.config(text = " La bonne réponse était - " + lQst[4] )
        false.pack()
        question.destroy()
        bouton1.destroy()
        bouton2.destroy()
        bouton3.destroy()
        valider.destroy()
        fermer.destroy()
 
def selection() :
      selection = "Vous avez choisi la réponse " + str(value.get())
      sel = Label(fenetre, text = selection)
      sel.pack()
 
qtal = Question #On enlève des éléments de la liste pour ne pas répéter la question.
 
x = 0
while x < 3 :
 
    new = Label(fenetre, text = "\n Nouvelle question : \n")
    new.pack()
 
    lQst = random.choice(qtal)  #On prend une question
    qtal.remove(lQst)  #Qu'on enlève de notre liste de questions
 
    value = StringVar()
    question = Label(fenetre, text = lQst[0])
    question.pack()
    bouton1 = Radiobutton(fenetre, text = lQst[1], variable = value, value = 'A', command = selection)
    bouton1.deselect()
    bouton2 = Radiobutton(fenetre, text = lQst[2], variable = value, value = 'B', command = selection)
    bouton2.deselect()
    bouton3 = Radiobutton(fenetre, text = lQst[3], variable = value, value = 'C', command = selection)
    bouton3.deselect()
    bouton1.pack()
    bouton2.pack()
    bouton3.pack()
 
 
    valider = Button(fenetre, text = "Valider", command = verification)
    valider.pack()
     
    fermer = Button(fenetre, text = "Fermer", command = fenetre.quit)
    fermer.pack()
         
    x += 1
         
end = Label(fenetre, text = " Fin du jeu ! ")
end.pack() 
 
fenetre.mainloop()