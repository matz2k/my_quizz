#!/usr/bin/env python3

from tkinter import *

def sel():
   selection = "\nVous avez sélectioné la réponse " + str(var.get())
   label.config(text = selection)

def close_window(): 
    root.destroy()


def q2(root, label, q_1, q_2, q_3, q_4):
	root.quit()
	root = Tk()
	root.title("QCM PROTO MATHIEU")
	label1 = Label(root, text="Bienvenue dans ce QCM", fg = "red", bg = "yellow")
	label1.pack()
	label['text'] = "Quel nom est le plus commun ?"
	q_1['text'] = "a) Kevin"
	q_1.pack()
	q_2['text'] = "a) Paul"
	q_2.pack()
	q_3['text'] = "a) Jacque"
	q_3.pack()
	q_4['text'] = "a) Henri"
	q_4.pack()

def q3(root, label, q_1, q_2, q_3, q_4):
	label['text'] = "2 + 2 = "
	q_1['text'] = "a) 3"
	q_1.pack()
	q_2['text'] = "a) 5"
	q_2.pack()
	q_3['text'] = "a) 6"
	q_3.pack()
	q_4['text'] = "a) 4"
	q_4.pack()

def q4(root, label, q_1, q_2, q_3, q_4):
	label['text'] = "A quel age somme nous adulte ?"
	q_1['text'] = "a) 13 ans"
	q_1.pack()
	q_2['text'] = "a) 20 ans"
	q_2.pack()
	q_3['text'] = "a) 16 ans"
	q_3.pack()
	q_4['text'] = "a) 18 ans"
	q_4.pack()

def q5(root, label, q_1, q_2, q_3, q_4):
	label['text'] = "Combien de couleur primaire existe t-il ?"
	q_1['text'] = "a) 2"
	q_1.pack()
	q_2['text'] = "a) 5"
	q_2.pack()
	q_3['text'] = "a) 3"
	q_3.pack()
	q_4['text'] = "a) 6"
	q_4.pack()

def q6(root, label, q_1, q_2, q_3, q_4):
	label['text'] = "A quelle age est mort Albert Einstein ?"
	q_1['text'] = "a) 64"
	q_1.pack()
	q_2['text'] = "a) 54"
	q_2.pack()
	q_3['text'] = "a) 59"
	q_3.pack()
	q_4['text'] = "a) 73"
	q_4.pack()
	

def make_the_dict(root, label, q_1, q_2, q_3, q_4):
	question_dict = dict()
	question_dict.setdefault(5, lambda: q6(root, label, q_1, q_2, q_3, q_4))
	question_dict.setdefault(4, lambda: q5(root, label, q_1, q_2, q_3, q_4))
	question_dict.setdefault(3, lambda: q4(root, label, q_1, q_2, q_3, q_4))
	question_dict.setdefault(2, lambda: q3(root, label, q_1, q_2, q_3, q_4))
	question_dict.setdefault(1, lambda: q2(root, label, q_1, q_2, q_3, q_4))
	return question_dict

global global_score
global_score = 0
reponse = 3
count = 1
root = Tk()
root.title("QCM PROTO MATHIEU")
label1 = Label(root, text="Bienvenue dans ce QCM", fg = "red", bg = "yellow")
label1.pack()
label = Label(root, text="Quel est le plus beau pays ?")
label.pack()
q_1_a = IntVar()
q_2_a = IntVar()
q_3_a = IntVar()
q_4_a = IntVar()
q_1 = Checkbutton(root, text="a) France", variable=q_1_a)
q_1.pack()
q_2 = Checkbutton(root, text="b) Zimbabwe", variable=q_2_a)
q_2.pack()
q_3 = Checkbutton(root, text="c) Costa rica", variable=q_3_a)
q_3.pack()
q_4 = Checkbutton(root, text="d) Pérou", variable=q_4_a)
q_4.pack()
question_dict = make_the_dict(root, label, q_1, q_2, q_3, q_4)
button_next = Button (root, text = "Question suivante", command = question_dict[count])
button_next.pack()
button_close = Button (root, text = "AStalabistaBABY", command = close_window)
button_close.pack()
print (q_1_a.get())
root.mainloop()
print (q_1_a.get())